/*
** my_getnbr.c for my_getnbr in /home/barbis_j/rendu/Piscine-C-Jour_04
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Oct  3 16:13:56 2013 barbis_j
** Last update Sat Jun  7 15:32:30 2014 da-sil_l
*/

int	body(char *str, int i, int num, int c)
{
  while (str[i] == '-' || str[i] == '+')
    {
      if (str[i] == '-')
	c = c + 1;
      i = i + 1;
      if (str[i] != '-' && str[i] != '+')
	if ((c % 2) == 1)
	  while (str[i] != '\0' && str[i] <= 57 && str[i] >=48)
	    {
	      num = (num  - (str[i] - 48)) * 10;
	      if (num > 0)
		return (0);
	      i = i + 1;
	    }
    }
  while (str[i] != '\0' && str[i] <= 57 && str[i] >= 48)
    {
      num = (num  + (str[i] - 48)) * 10;
      if (num < 0)
        return (0);
      i = i + 1;
    }
  num = num / 10;
  return (num);
}

int	my_getnbr(char *str)
{
  int	i;
  int	num;
  int	c;

  i = 0;
  num = 0;
  c = 0;
  if (str && str[0])
    return (body(str, i, num, c));
  return (0);
}
