/*
** my_check_k.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Thu Mar 13 12:32:02 2014 
** Last update Sat Jun  7 12:00:35 2014 
*/

#include "my.h"

void		init_value(t_rtv *rtv, int color, double mat, double k)
{
  rtv->color.mat = mat;
  rtv->lux.k = k;
  rtv->color.d = color;
  rtv->color.red = rtv->k.red[color];
  rtv->color.blue = rtv->k.blue[color];
  rtv->color.green = rtv->k.green[color];
}

void		init_check(t_rtv *rtv)
{
  if (rtv->k.k[SPHERE] < 0.00001 && rtv->k.k[PLAN] < 0.00001)
    {
      rtv->color.d = 4;
      rtv->lux.k = 1;
    }
  else if (rtv->k.k[SPHERE] <= 0.00001)
    init_value(rtv, PLAN, rtv->k.mat[PLAN], rtv->k.k[PLAN]);
  else if (rtv->k.k[PLAN] <= 0.00001)
    init_value(rtv, SPHERE, rtv->k.mat[SPHERE], rtv->k.k[SPHERE]);
  else if (rtv->k.k[PLAN] <= rtv->k.k[SPHERE])
    init_value(rtv, PLAN, rtv->k.mat[PLAN], rtv->k.k[PLAN]);
  else if (rtv->k.k[SPHERE] <= rtv->k.k[PLAN])
    init_value(rtv, SPHERE, rtv->k.mat[SPHERE], rtv->k.k[SPHERE]);
}

void		check_all_k(t_rtv *rtv)
{
  init_check(rtv);
  if (rtv->k.k[CYLINDRE] < rtv->lux.k)
    init_value(rtv, CYLINDRE, rtv->k.mat[CYLINDRE], rtv->k.k[CYLINDRE]);
  if (rtv->k.k[CONE] < rtv->lux.k)
    init_value(rtv, CONE, rtv->k.mat[CONE], rtv->k.k[CONE]);
  line_param(rtv);
  my_choose_color(rtv, rtv->lux.k);
}
