/*
** memset_struct.c for  in /home/calvez_k/Dropbox/Raytracer/Bego
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri May 16 20:48:51 2014 
** Last update Sun Jun  8 15:20:09 2014 
*/

#include "my.h"

void	memset_struct_other(t_rtv *rtv)
{
  rtv->other.x = 0;
  rtv->other.shadow = 0;
}

void	memset_struct_k(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  while(nb <= 10)
    {
      rtv->k.mat[nb] = 0;
      rtv->k.red[nb] = 0;
      rtv->k.green[nb] = 0;
      rtv->k.blue[nb] = 0;
      rtv->k.k[nb] = 0;
      nb += 1;
    }
}

void	memset_struct_sphere(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->sphere.nb = 0;
  rtv->sphere.a = 0;
  rtv->sphere.b = 0;
  rtv->sphere.c = 0;
  rtv->sphere.delta = 0;
  rtv->sphere.k = 0;
  rtv->sphere.d = 0;
  while(nb <= 10)
    {
      rtv->sphere.rayon[nb] = 0;
      rtv->sphere.x[nb] = 0;
      rtv->sphere.y[nb] = 0;
      rtv->sphere.z[nb] = 0;
      rtv->sphere.rotx[nb] = 0;
      rtv->sphere.roty[nb] = 0;
      rtv->sphere.rotz[nb] = 0;
      rtv->sphere.red[nb] = 0;
      rtv->sphere.green[nb] = 0;
      rtv->sphere.blue[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_t_rtv(t_rtv *rtv)
{
  memset_struct_sphere(rtv);
  memset_struct_shadow(rtv);
  memset_struct_cyl(rtv);
  memset_struct_cone(rtv);
  memset_struct_k(rtv);
  memset_struct_color(rtv);
  memset_struct_plan(rtv);
  memset_struct_lux(rtv);
  memset_struct_normal(rtv);
  memset_struct_light(rtv);
  memset_struct_dot(rtv);
  memset_struct_eye(rtv);
  memset_struct_cp(rtv);
  memset_struct_other(rtv);
  memset_struct_cos(rtv);
  memset_struct_spot(rtv);
}
