/*
** init_sphere.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri Mar 14 17:32:48 2014 
** Last update Sun Jun  8 15:17:59 2014 
*/

#include <math.h>
#include "my.h"

void		two_delta(t_rtv *rtv, int nb)
{
  double	k1;
  double	k2;
  double	k;

  k = 2000;
  k1 = (((-1) * rtv->sphere.b) + sqrt(rtv->sphere.delta)) / (2 * rtv->sphere.a);
  k2 = (((-1) *rtv->sphere.b) - sqrt(rtv->sphere.delta)) / (2 * rtv->sphere.a);
  if (k1 < k2 && k1 >= 0.00001)
    k = k1;
  else if (k2 < k1 && k2 >= 0.00001)
    k = k2;
  if (k < rtv->k.k[SPHERE])
    {
      rtv->k.k[SPHERE] = k;
      rtv->k.mat[SPHERE] = rtv->sphere.mat[nb];
      rtv->k.red[SPHERE] = rtv->sphere.red[nb];
      rtv->k.green[SPHERE] = rtv->sphere.green[nb];
      rtv->k.blue[SPHERE] = rtv->sphere.blue[nb];
    }
}

void		fct_sphere(t_rtv *rtv, int nb)
{
  rtv->cp.x = rtv->line.vx + rtv->sphere.x[nb];
  rtv->cp.y = rtv->line.vy + rtv->sphere.y[nb];
  rtv->cp.z = rtv->line.vz + rtv->sphere.z[nb];
  rtv->sphere.a = (pow(rtv->cp.x, 2) +
		   pow(rtv->cp.y, 2) +
		   pow(rtv->cp.z, 2));
  rtv->sphere.b = 2 * ((rtv->eye.x * rtv->cp.x) +
		       (rtv->eye.y * rtv->cp.y) +
		       (rtv->eye.z * rtv->cp.z));
  rtv->sphere.c = (pow(rtv->eye.x, 2) +
		   pow(rtv->eye.y, 2) +
		   pow(rtv->eye.z, 2) -
		   rtv->sphere.rayon[nb]);
  rtv->sphere.delta = pow(rtv->sphere.b, 2) -
    4 * rtv->sphere.a * rtv->sphere.c;
  if (rtv->sphere.delta >= 0)
    two_delta(rtv, nb);
}

void		init_sphere(t_rtv *rtv)
{
  int		nb;

  nb = 0;
  while (nb < rtv->sphere.nb)
    {
      fct_sphere(rtv, nb);
      nb = nb + 1;
    }
}
