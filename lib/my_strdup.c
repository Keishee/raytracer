/*
** my_strdup.c for strdup in /home/da-sil_l/rendu/Piscine-C-Jour_08
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Oct 10 09:34:55 2013 da-sil_l
** Last update Thu Jun  5 17:31:15 2014 guillaume
*/

#include <stdlib.h>
#include "my.h"

char	*my_strdup(char *src)
{
  char	*dest;
  int	i;

  i = 0;
  dest = xmalloc((my_strlen(src) + 1) * sizeof(char));
  while (src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = 0;
  return (dest);
}
