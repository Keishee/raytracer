/*
** plan.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar 14 15:33:42 2014 barbis_j
** Last update Fri Jun  6 20:53:14 2014 
*/

#include <math.h>
#include "my.h"
#include "rtv1.h"

void		init_cylindre(t_coord_b *eye_tmp,
			      t_coord_b *vec_tmp, t_obj *obj)
{
  eye_tmp->x = obj->eye.x;
  eye_tmp->y = obj->eye.y;
  eye_tmp->z = obj->eye.z;
  vec_tmp->x = obj->vec.x;
  vec_tmp->y = obj->vec.y + 100;
  vec_tmp->z = obj->vec.z;
  rtv1_mat_roty(eye_tmp, 90.0);
  rtv1_mat_roty(vec_tmp, 90.0);
  rtv1_mat_rotx(eye_tmp, -50.0);
  rtv1_mat_rotx(vec_tmp, -50.0);
}

void		lum_cylindre(t_lum *lum, t_obj *obj)
{
  lum->int_cylindre.x = obj->eye.x + obj->k.k[CYLINDRE] * obj->vec.x;
  lum->int_cylindre.y = obj->eye.y + obj->k.k[CYLINDRE] * obj->vec.y;
  lum->int_cylindre.z = obj->eye.z + obj->k.k[CYLINDRE] * obj->vec.z;
}

int		rtv1_cylindre(t_obj *obj, t_lum *lum)
{
  t_delta_b	delta;
  t_coord_b	eye_tmp;
  t_coord_b	vec_tmp;

  init_cylindre(&eye_tmp, &vec_tmp, obj);
  delta.a = pow(vec_tmp.x, 2) + pow(vec_tmp.y, 2);
  delta.b = 2.0 * (eye_tmp.x * vec_tmp.x + eye_tmp.y * vec_tmp.y);
  delta.c = pow(eye_tmp.x, 2) + pow(eye_tmp.y, 2) - pow(obj->ray, 2);
  delta.delta = pow(delta.b, 2) - 4.0 * delta.a * delta.c;
  if (delta.delta >= 0)
    {
      delta.k1 = (-delta.b + sqrt(delta.delta)) / (2.0 * delta.a);
      delta.k2 = (-delta.b - sqrt(delta.delta)) / (2.0 * delta.a);
    }
  else
    {
      obj->k.k[CYLINDRE] = -1.0;
      return (-1);
    }
  obj->k.k[CYLINDRE] = k_select(delta.k1, delta.k2);
  lum_cylindre(lum, obj);
  return (-1);
}
