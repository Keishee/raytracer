/*
** my_cone.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Thu Mar 13 18:07:05 2014 
** Last update Fri Jun  6 20:56:52 2014 
*/

#include <math.h>
#include "my.h"

void		two_delta_cone(t_rtv *rtv, int nb)
{
  double	k1;
  double	k2;
  double	k;

  k = 2000;
  k1 = (((-1) * rtv->cone.b) + sqrt(rtv->cone.delta)) / (2 * rtv->cone.a);
  k2 = (((-1) *rtv->cone.b) - sqrt(rtv->cone.delta)) / (2 * rtv->cone.a);
  if (k1 < k2 && k1 >= 0.00001)
    k = k1;
  else if (k2 < k1 && k2 >= 0.00001)
    k = k2;
  if (k < rtv->k.k[CONE])
    {
      rtv->k.k[CONE] = k;
      rtv->k.mat[CONE] = rtv->cone.mat[nb];
      rtv->k.red[CONE] = rtv->cone.red[nb];
      rtv->k.green[CONE] = rtv->cone.green[nb];
      rtv->k.blue[CONE] = rtv->cone.blue[nb];
    }
}

void		fct_cone(t_rtv *rtv, int nb, double rayon)
{
  rtv->cp.z = rtv->line.vz + rtv->cone.z[nb];
  rtv->cp.y = rtv->line.vy + rtv->cone.y[nb];
  rtv->cp.x = rtv->line.vx + rtv->cone.x[nb];
  rtv->cone.a = (pow(rtv->cp.x, 2) +
		 pow(rtv->cp.y, 2) -
		 pow(rtv->cp.z, 2) * (rayon / 10));
  rtv->cone.b = 2 * ((rtv->eye.x * rtv->cp.x) +
		     (rtv->eye.y * rtv->cp.y) +
		     (rtv->eye.z * rtv->cp.z));
  rtv->cone.c = (pow(rtv->eye.x, 2) +
  		 pow(rtv->eye.y, 2) +
  		 pow(rtv->eye.z, 2) * (rayon / 10));
  rtv->cone.delta = pow(rtv->cone.b, 2) - 4.0 * rtv->cone.a * rtv->cone.c;
  if (rtv->cone.delta >= 0)
    {
      two_delta_cone(rtv, nb);
      rtv->color.d = 2;
    }
}

void		init_cone(t_rtv *rtv)
{
  int		nb;

  nb = 0;
  while (nb < rtv->cone.nb)
    {
      init_rotate(rtv, rtv->cone.rotx[nb], rtv->cone.roty[nb],
		  rtv->cone.rotz[nb]);
      fct_cone(rtv, nb, rtv->cone.rayon[nb]);
      nb = nb + 1;
      vision_line(rtv);
    }
}
