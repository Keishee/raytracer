/*
** plan.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar 14 16:17:06 2014 barbis_j
** Last update Fri Mar 14 17:46:44 2014 barbis_j
*/

#include <math.h>
#include "rtv1.h"

void		init_plan(t_coord *eye_tmp, t_coord *vec_tmp, t_obj *obj)
{
  eye_tmp->x = obj->eye.x;
  eye_tmp->y = obj->eye.y;
  eye_tmp->z = obj->eye.z + 50.0;
  vec_tmp->x = obj->vec.x;
  vec_tmp->y = obj->vec.y;
  vec_tmp->z = obj->vec.z;
  //rtv1_mat_roty(eye_tmp, 90.0);
  //rtv1_mat_roty(vec_tmp, 90.0);
}

void		lum_plan(t_lum *lum, t_obj *obj)
{
  lum->int_plan.x = obj->eye.x + obj->k.k[PLAN] * obj->vec.x;
  lum->int_plan.y = obj->eye.y + obj->k.k[PLAN] * obj->vec.y;
  lum->int_plan.z = obj->eye.z + obj->k.k[PLAN] * obj->vec.z;
}

int		rtv1_plan(t_obj *obj, t_lum *lum)
{
  double	b;
  double	c;
  double	delta;
  t_coord	eye_tmp;
  t_coord	vec_tmp;

  init_plan(&eye_tmp, &vec_tmp, obj);
  if ((b = vec_tmp.z) == 0.0)
    {
      obj->k.k[PLAN] = -1.0;
      return (-1);
    }
  c = eye_tmp.z;
  obj->k.k[PLAN] = -(c / b);
  lum_plan(lum, obj);
  if (obj->k.k[PLAN] < 0.00001)
    {
      obj->k.k[PLAN] = -1.0;
      return (-1);
    }
}
