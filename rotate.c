/*
** rotate.c for rotatz in /home/dana_a/rendu/TP-RT-1
** 
** Made by dana_a
** Login   <dana_a@epitech.net>
** 
** Started on  Sat Feb 15 13:55:37 2014 dana_a
** Last update Sat Jun  7 19:01:23 2014 
*/

#include <math.h>
#include "my.h"

void		rotate_x(t_rtv *rtv, double a)
{
  double	ty;
  double	tz;

  if ((a = (a * M_PI) / 180.0) == 0)
    return ;
  ty = (rtv->line.vy * cos(a)) + (rtv->line.vz * - sin(a));
  tz = (rtv->line.vz * cos(a)) + (rtv->line.vy * sin(a));
  rtv->line.vy = ty;
  rtv->line.vz = tz;
}

void		rotate_y(t_rtv *rtv, double a)
{
  double	tx;
  double	tz;

  if ((a = (a * M_PI) / 180.0) == 0)
    return ;
  tx = (rtv->line.vx * cos(a)) + (rtv->line.vz * sin(a));
  tz = (rtv->line.vx * -sin(a)) + (rtv->line.vz * cos(a));
  rtv->line.vx = tx;
  rtv->line.vz = tz;
}

void		rotate_z(t_rtv *rtv, double a)
{
  double	tx;
  double	ty;

  if ((a = (a * M_PI) / 180.0) == 0)
    return ;
  tx = (rtv->line.vx * cos(a)) + (rtv->line.vy * - sin(a));
  ty = (rtv->line.vx * sin(a)) + (rtv->line.vy * cos(a));
  rtv->line.vx = tx;
  rtv->line.vy = ty;
}

void	init_rotate(t_rtv *rtv, double x, double y, double z)
{
  rotate_x(rtv, x);
  rotate_y(rtv, y);
  rotate_z(rtv, z);
}
