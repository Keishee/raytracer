/*
** get_next_line.h for dko in /home/da-sil_l/rendu/gnl
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Tue Nov 19 13:53:33 2013 da-sil_l
** Last update Mon Jun  2 14:02:17 2014 Clément
*/

#ifndef GET_NEXT_LINE_
# define GET_NEXT_LINE_

# define BUFF 20

char	*my_realloc(char *, int);
void	*xmalloc(int);
char	*get_next_line(const int fd);

#endif /* !GET_NEXT_LINE */
