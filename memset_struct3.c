/*
** memset_struct3.c for rt in /home/clement/Dropbox/Raytracer/Clem
** 
** Made by Clément
** Login   <clement@epitech.net>
** 
** Started on  Mon Jun  2 14:06:50 2014 Clément
** Last update Thu Jun  5 16:30:11 2014 guillaume
*/

#include "my.h"

void	memset_struct_dot(t_rtv *rtv)
{
  rtv->dot.x = 0;
  rtv->dot.y = 0;
  rtv->dot.z = 0;
}

void	memset_struct_eye(t_rtv *rtv)
{
  rtv->eye.x = 0;
  rtv->eye.y = 0;
  rtv->eye.z = 0;
}

void	memset_struct_cp(t_rtv *rtv)
{
  rtv->cp.x = 0;
  rtv->cp.y = 0;
  rtv->cp.z = 0;
}

void	memset_struct_normal(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->normal.nb = 0;
  rtv->normal.k = 0;
  rtv->normal.p = 0;
  while(nb <= 10)
    {
      rtv->normal.x[nb] = 0;
      rtv->normal.y[nb] = 0;
      rtv->normal.z[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_light(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->light.nb = 0;
  rtv->light.k = 0;
  rtv->light.p = 0;
  while(nb <= 10)
    {
      rtv->light.x[nb] = 0;
      rtv->light.y[nb] = 0;
      rtv->light.z[nb] = 0;
      nb += 1;
  }
}
