/*
** my_color.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Wed Feb 12 15:12:24 2014 
** Last update Sun Jun  8 17:15:41 2014 
*/

#include "my.h"

double		calcul_cos_spot(t_rtv *rtv)
{
  int		nb;
  double	cos;

  nb = 0;
  cos = 0;
  while (nb <= rtv->spot.nb)
    {
      cos += rtv->cos.cos[nb];
      nb = nb + 1;
    }
  return (cos);
}

void		calcul_color_spot(t_rtv *rtv, int color)
{
  int		nb;
  double	tmp;

  rtv->spot.color[color] = 0;
  tmp = 0;
  nb = 0;
  while (nb <= rtv->spot.nb)
    {
      tmp = (rtv->color.mat * rtv->spot.colore[color][nb] *
		rtv->cos.cos[nb]);
      rtv->spot.color[color] += tmp;
      nb = nb + 1;
    }
}

double		calcul_spot(t_rtv *rtv, int color)
{
  int		nb;
  double	ret;
  double	tmp;

  ret = 1;
  tmp = 0;
  nb = 0;
  while (nb < rtv->spot.nb)
    {
      tmp = rtv->spot.colore[color][nb];
      ret *= tmp;
      nb = nb + 1;
    }
  ret /= nb - 1;
  return (ret);
}

void		my_color(t_rtv *rtv)
{
  double	cos;

  cos = calcul_cos_spot(rtv);
  calcul_color_spot(rtv, 2);
  rtv->color.blue = rtv->color.blue * cos + rtv->spot.color[2];
  calcul_color_spot(rtv, 1);
  rtv->color.green = rtv->color.green * cos + rtv->spot.color[1];
  calcul_color_spot(rtv, 0);
  rtv->color.red = rtv->color.red * cos + rtv->spot.color[0];
  if (rtv->color.red >= 255)
     rtv->color.red = 255;
  if (rtv->color.green >= 255)
    rtv->color.green = 255;
  if (rtv->color.blue >= 255)
    rtv->color.blue = 255;
  if (rtv->other.shadow == 1)
    init_shadow(rtv);
}

void		my_void(t_rtv *rtv, int red, int green, int blue)
{
  rtv->color.blue = blue;
  rtv->color.green = green;
  rtv->color.red = red;
}

void		my_choose_color(t_rtv *rtv, double k)
{
  if (rtv->color.d == 4)
    my_void(rtv, 0, 0, 0);
  else
    my_color(rtv);
}
