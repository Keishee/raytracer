##
## Makefile for  in /home/calvez_k/rendu/MUL_2013_rtv1
## 
## Made by 
## Login   <calvez_k@epitech.net>
## 
## Started on  Fri Feb 21 12:16:21 2014 
## Last update Sun Jun  8 15:21:38 2014 
##

CC	= gcc -g

RM	= rm -f

CFLAGS	+= -I.

NAME	= rt

SRC	= main.c		\
	  parsing_conf.c	\
	  parsing_plan.c	\
	  parsing_object.c	\
	  gere_key.c		\
	  loading.c		\
	  memset_struct.c	\
	  memset_struct2.c	\
	  memset_struct3.c	\
	  memset_struct4.c	\
	  init_img.c		\
	  init_sphere.c		\
	  inter_plan.c		\
	  fill.c		\
	  my_color.c		\
	  my_shadow.c		\
	  my_cylindre.c		\
	  my_check_k.c		\
	  my_spot.c		\
	  my_cone.c		\
	  rotate.c		\
	  make_video.c		\
	  lib/get_next_line.c	\
	  lib/my_strncpy.c	\
	  lib/my_putnbr.c	\
	  lib/my_getnbr.c	\
	  lib/my_strdup.c	\
	  lib/my_strcmp.c	\
	  lib/wordtab.c		\
	  lib/xmalloc.c		\
	  lib/my_strlen.c	\
	  misc/calc.c		\
	  misc/cylindre.c	\
	  misc/image.c		\
	  misc/k_finder.c	\
	  misc/light.c		\
	  misc/math.c		\
	  misc/sphere.c


LIBX	= -L/usr/lib64 -lmlx_$(HOSTTYPE) -L/usr/lib64/X11 -lXext -lX11 -lm

OBJ	= $(SRC:.c=.o)


all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LIBX)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
