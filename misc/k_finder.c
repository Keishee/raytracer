/*
** k_finder.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Feb 20 14:55:01 2014 barbis_j
** Last update Fri Mar 14 18:06:14 2014 barbis_j
*/

#include "rtv1.h"

int		k_min(t_obj *obj)
{
  int		i;
  int		min;

  i = 0;
  min = 5;
  while (i < 4)
    {
      if (obj->k.k[i] >= 0.00001 && obj->k.k[i] < obj->k.k[min])
	min = i;
      ++i;
    }
  if (min == 5)
    return (-1);
  return (min);
}

double		k_select(double k1, double k2)
{
  if (k1 < 0 && k2 < 0)
    return (-1.0);
  if (k1 < 0)
    return (k2);
  if (k2 < 0)
    return (k1);
  if (k1 <= k2)
    return (k1);
  else
    return (k2);
}
