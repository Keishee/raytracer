/*
** xmalloc.c for xmalloc in /home/barbis_j/rendu/Piscine-C-lib/my
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Nov  5 16:15:48 2013 barbis_j
** Last update Fri Apr 18 14:25:18 2014 
*/

#include <unistd.h>
#include <stdlib.h>

void	*xmalloc(int i)
{
  void	*ptr;

  ptr = malloc(i);
  if (ptr == NULL)
    {
      write(2, "Can't perform malloc\n", 21);
      exit(-1);
    }
  return (ptr);
}
