/*
** rtv1.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Wed Feb  5 15:03:56 2014 
** Last update Sun Jun  8 18:27:20 2014 barbis_j
*/

#include <mlx.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"

int		new_win(t_win *win)
{
  if ((win->mlx_ptr = mlx_init()) == 0)
    return (0);
  if ((win->win_ptr =
       mlx_new_window(win->mlx_ptr, WINX, WINY, "Raytracer")) == 0)
    return (0);
  return (1);
}

int		new_img(t_win *win)
{
  if ((win->img_ptr[win->nb] =
       mlx_new_image(win->mlx_ptr, WINX, WINY)) == NULL)
    return (0);
  if ((win->data[win->nb] = mlx_get_data_addr(win->img_ptr[win->nb],
  					  &win->bpp[win->nb],
  					  &win->sizeline[win->nb],
  					  &win->endian[win->nb])) == NULL)
    return (0);
  return (1);
}

int		fill_tab_str(t_win *win, char *file)
{
  int		fd;
  int		nb;

  if (file != NULL)
    {
      if ((fd = open(file, O_RDONLY)) == -1)
	exit(1);
    }
  else
    {
      if ((fd = open("scene/scene.txt", O_RDONLY)) == -1)
	exit(1);
    }
  nb = 0;
  while ((win->str[nb] = get_next_line(fd)) != NULL)
    ++nb;
  close(fd);
  return (nb);
}

void	malloc_my_tab(t_win *win)
{
  win->img_ptr = xmalloc(NB_VIDEO * sizeof(void *));
  win->sizeline = xmalloc(NB_VIDEO * sizeof(int));
  win->data = xmalloc(NB_VIDEO * sizeof(char *));
  win->bpp = xmalloc(NB_VIDEO * sizeof(int));
  win->endian = xmalloc(NB_VIDEO * sizeof(int));
  win->str = xmalloc(NB_VIDEO * sizeof(char *));
}

int	main(int ac, char **av)
{
  t_win	win;

  if ((new_win(&win)) == 0)
    return (0);
  malloc_my_tab(&win);
  win.nb = 0;
  win.num = 0;
  win.vid = 0;
  if (ac >= 2 && my_strcmp(av[1], "-video") == 0)
    init_video(&win);
  else
    {
      win.num_min_img = fill_tab_str(&win, av[1]);
      if (win.num_min_img > NB_IMG)
	win.num_min_img = NB_IMG;
      else
	while (win.nb < win.num_min_img)
	  init_img(&win);
    }
  mlx_hook(win.win_ptr, 2, 1, gere_key, &win);
  mlx_expose_hook(win.win_ptr, gere_expose, &win);
  mlx_loop(win.mlx_ptr);
  return (0);
}
