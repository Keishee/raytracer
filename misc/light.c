/*
** light.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Feb 20 16:13:13 2014 barbis_j
** Last update Fri Jun  6 16:58:19 2014 da-sil_l
*/

#include <math.h>
#include "rtv1.h"

void		light_math(t_coord_b *p, t_coord_b *vlum,
			   double *cos_a, double *cos_b)
{
  t_coord_b	spot;
  t_coord_b	spot2;

  *cos_a = 0.0;
  *cos_b = 0.0;
  spot.x = -400.0;
  spot.y = -200.0;
  spot.z = 250.0;
  spot2.x = -400.0;
  spot2.y = 200.0;
  spot2.z = 250.0;
  vlum->x = spot.x - p->x;
  vlum->y = spot.y - p->y;
  vlum->z = spot.z - p->z;
  *cos_a = p->x * vlum->x + p->y * vlum->y + p->z * vlum->z;
  vlum->x = spot2.x - p->x;
  vlum->y = spot2.y - p->y;
  vlum->z = spot2.z - p->z;
  *cos_b = p->x * vlum->x + p->y * vlum->y + p->z * vlum->z;
}

void		light(t_color_b *color, t_coord_b *p, int flag)
{
  double	cos_a;
  double	cos_b;
  t_coord_b	vlum;

  light_math(p, &vlum, &cos_a, &cos_b);
  if (cos_a < 0)
    cos_a = 0;
  if (cos_a > 1)
    cos_a = cos_a / 30000.0;
  if (cos_b < 0)
    cos_b = 0;
  if (cos_b > 1)
    cos_b = cos_b / 30000.0;
  cos_a = (cos_b + cos_a) / 2.0;
  if (cos_a < 0.3)
    cos_a = 0.3;
  if ((color->r = color->r * cos_a) > 255)
    color->r = 255.0;
  if ((color->g = color->g * cos_a) > 255)
    color->g = 255.0;
  if ((color->b = color->b * cos_a) > 255)
    color->b = 255.0;
}
