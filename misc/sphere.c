/*
** ne sphere.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar 14 14:28:41 2014 barbis_j
** Last update Fri Jun  6 15:17:06 2014 da-sil_l
*/

#include <math.h>
#include "my.h"
#include "rtv1.h"

void		init_sphere_b(t_coord_b *eye_tmp, t_coord_b *vec_tmp, t_obj *obj)
{
  eye_tmp->x = obj->eye.x;
  eye_tmp->y = obj->eye.y;
  eye_tmp->z = obj->eye.z;
  vec_tmp->x = obj->vec.x;
  vec_tmp->y = obj->vec.y + 120.0;
  vec_tmp->z = obj->vec.z;
}

void		lum_sphere(t_lum *lum, t_obj *obj)
{
  lum->int_sphere.x = obj->eye.x + obj->k.k[SPHERE] * obj->vec.x;
  lum->int_sphere.y = obj->eye.y + obj->k.k[SPHERE] * obj->vec.y;
  lum->int_sphere.z = obj->eye.z + obj->k.k[SPHERE] * obj->vec.z;
}

int		rtv1_sphere(t_obj *obj, t_lum *lum)
{
  t_delta_b	delta;
  t_coord_b	eye_tmp;
  t_coord_b	vec_tmp;

  init_sphere_b(&eye_tmp, &vec_tmp, obj);
  delta.a = pow(vec_tmp.x, 2) + pow(vec_tmp.y, 2) + pow(vec_tmp.z, 2);
  delta.b = 2.0 * (eye_tmp.x * vec_tmp.x + eye_tmp.y *
		   vec_tmp.y + eye_tmp.z * vec_tmp.z);
  delta.c = pow(eye_tmp.x, 2) + pow(eye_tmp.y, 2) +
    pow(eye_tmp.z, 2) - pow(120, 2);
  delta.delta = pow(delta.b, 2) - 4.0 * delta.a * delta.c;
  if (delta.delta >= 0)
    {
      delta.k1 = (-delta.b + sqrt(delta.delta)) / (2.0 * delta.a);
      delta.k2 = (-delta.b - sqrt(delta.delta)) / (2.0 * delta.a);
    }
  else
    {
      obj->k.k[SPHERE] = -1.0;
      return (-1);
    }
  obj->k.k[SPHERE] = k_select(delta.k1, delta.k2);
  lum_sphere(lum, obj);
  return (-1);
}
