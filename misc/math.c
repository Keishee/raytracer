/*
** math.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat Feb 15 14:34:19 2014 barbis_j
** Last update Fri Jun  6 15:15:58 2014 da-sil_l
*/

#include <math.h>
#include "rtv1.h"

void		rtv1_mat_rotx(t_coord_b *co, double angle)
{
  t_coord_b	co_tmp;

  angle = angle * M_PI / 180.0;
  co_tmp.y = -sin(angle) * co->z + co->y * cos(angle);
  co_tmp.z = cos(angle) * co->z + sin(angle) * co->y;
  co->y = co_tmp.y;
  co->z = co_tmp.z;
}

void		rtv1_mat_roty(t_coord_b *co, double angle)
{
  t_coord_b	co_tmp;

  angle = angle * M_PI / 180.0;
  co_tmp.x = co->x * cos(angle) + co->z * sin(angle);
  co_tmp.z = co->x * -sin(angle) + co->z * cos(angle);
  co->x = co_tmp.x;
  co->z = co_tmp.z;
}

void		rtv1_mat_rotz(t_coord_b *co, double angle)
{
  t_coord_b	co_tmp;

  angle = angle * M_PI / 180.0;
  co_tmp.x = co->y * -sin(angle) + co->x * cos(angle);
  co_tmp.y = co->y * cos(angle) + co->x * sin(angle);
  co->x = co_tmp.x;
  co->y = co_tmp.y;
}
