/*
** wordtab.h for wordtab in /home/barbis_j/Documents/Projets/wordtab
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Dec  9 14:10:23 2013 barbis_j
** Last update Thu Jun  5 17:33:51 2014 guillaume
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

# define COND(c) (((c) == ',' || (c) == ' ') ? 1 : 0)

void		*xmalloc(int);
char		*my_strncpy(char *, char *, int);

#endif /* !WORDTAB_H_ */
