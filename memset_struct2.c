/*
** memset_struct2.c for rt in /home/clement/Dropbox/Raytracer/Clem
** 
** Made by Clément
** Login   <clement@epitech.net>
** 
** Started on  Mon Jun  2 14:05:41 2014 Clément
** Last update Sun Jun  8 15:20:25 2014 
*/

#include "my.h"

void	memset_struct_shadow(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->shadow.nb = 0;
  rtv->shadow.a = 0;
  rtv->shadow.b = 0;
  rtv->shadow.c = 0;
  rtv->shadow.delta = 0;
  rtv->shadow.k = 20;
  rtv->shadow.d = 0;
  while(nb <= 10)
    {
      rtv->shadow.rayon[nb] = 0;
      rtv->shadow.x[nb] = 0;
      rtv->shadow.y[nb] = 0;
      rtv->shadow.z[nb] = 0;
      rtv->shadow.rotx[nb] = 0;
      rtv->shadow.roty[nb] = 0;
      rtv->shadow.rotz[nb] = 0;
      rtv->shadow.red[nb] = 0;
      rtv->shadow.green[nb] = 0;
      rtv->shadow.blue[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_cyl(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->cyl.nb = 0;
  rtv->cyl.a = 0;
  rtv->cyl.b = 0;
  rtv->cyl.c = 0;
  rtv->cyl.delta = 0;
  rtv->cyl.k = 0;
  rtv->cyl.d = 0;
  while(nb <= 10)
    {
      rtv->cyl.rayon[nb] = 0;
      rtv->cyl.x[nb] = 0;
      rtv->cyl.y[nb] = 0;
      rtv->cyl.z[nb] = 0;
      rtv->cyl.rotx[nb] = 0;
      rtv->cyl.roty[nb] = 0;
      rtv->cyl.rotz[nb] = 0;
      rtv->cyl.red[nb] = 0;
      rtv->cyl.green[nb] = 0;
      rtv->cyl.blue[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_cone(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->cone.nb = 0;
  rtv->cone.a = 0;
  rtv->cone.b = 0;
  rtv->cone.c = 0;
  rtv->cone.delta = 0;
  rtv->cone.k = 0;
  rtv->cone.d = 0;
  while(nb <= 10)
    {
      rtv->cone.rayon[nb] = 0;
      rtv->cone.x[nb] = 0;
      rtv->cone.y[nb] = 0;
      rtv->cone.z[nb] = 0;
      rtv->cone.rotx[nb] = 0;
      rtv->cone.roty[nb] = 0;
      rtv->cone.rotz[nb] = 0;
      rtv->cone.red[nb] = 0;
      rtv->cone.green[nb] = 0;
      rtv->cone.blue[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_cos(t_rtv *rtv)
{
  int	nb;
  int	nb_nb;

  nb = 0;
  rtv->cos.nb = 0;
  rtv->cos.intensite = 0;
  while(nb <= 10)
    {
      rtv->cos.x[nb] = 0;
      rtv->cos.y[nb] = 0;
      rtv->cos.z[nb] = 0;
      rtv->cos.color[nb] = 0;
      rtv->cos.cos[nb] = 0;
      rtv->cos.red[nb] = 0;
      rtv->cos.green[nb] = 0;
      rtv->cos.blue[nb] = 0;
      nb_nb = 0;
      while (nb_nb <= 10)
	{
	  rtv->cos.colore[nb][nb_nb] = 0;
	  ++nb_nb;
	}
      nb += 1;
  }
}

void	memset_struct_spot(t_rtv *rtv)
{
  int	nb;
  int	nb_nb;

  nb = 0;
  rtv->spot.nb = 0;
  rtv->spot.intensite = 0;
  while(nb <= 10)
    {
      rtv->spot.x[nb] = 0;
      rtv->spot.y[nb] = 0;
      rtv->spot.z[nb] = 0;
      rtv->spot.color[nb] = 0;
      rtv->spot.cos[nb] = 0;
      rtv->spot.red[nb] = 0;
      rtv->spot.green[nb] = 0;
      rtv->spot.blue[nb] = 0;
      nb_nb = 0;
      while (nb_nb <= 10)
	{
	  rtv->spot.colore[nb][nb_nb] = 0;
	  ++nb_nb;
	}
      nb += 1;
  }
}
