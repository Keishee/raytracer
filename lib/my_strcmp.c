/*
** my_strcmp.c for my_strcmp in /home/da-sil_l/rendu/Piscine-C-Jour_06/ex_05
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Tue Oct  8 16:55:10 2013 da-sil_l
** Last update Mon Jun  2 14:16:06 2014 Clément
*/

int	my_strcmp(const char *s1, const char *s2)
{
  int	i;

  i = 0;
  while (s1[i] != 0 && s2[i] != 0 && s1[i] == s2[i])
    ++i;
  return (s1[i] - s2[i]);
}
