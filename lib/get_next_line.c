/*
** get_next_line.c for dpfk in /home/da-sil_l/rendu/CPE_2013_getnextline/test
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Wed Nov 20 18:29:19 2013 da-sil_l
** Last update Thu Jun  5 15:03:57 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"

static char	*my_gn_realloc(char *str, int i)
{
  char		*result;
  int		j;

  j = 0;
  if ((result = malloc(i * sizeof(char *))) == NULL)
    return (NULL);
  while (i >= 0)
    {
      result[j] = str[j];
      --i;
      ++j;
    }
  free(str);
  return (result);
}

static char	fill_my_buffer(int *ret, int fd, char *buffer, int *i)
{
  if (*ret == 0)
    {
      *i = 0;
      *ret = read(fd, buffer, BUFF);
    }
  return (*buffer);
}

char		*get_next_line(const int fd)
{
  char  	*result;
  static char	buffer[BUFF];
  static int	i;
  int		j;
  static int   	ret;

  j = 0;
  if (buffer[i++] == 10)
    --ret;
  *buffer = fill_my_buffer(&ret, fd, buffer, &i);
  if (ret <= 0)
    return (NULL);
  if ((result = malloc(sizeof(char *))) == NULL)
    return (NULL);
  while (buffer[i] != 0 && buffer[i] != '\n' && ret-- != 0)
    {
      result[j++] = buffer[i++];
      if ((result = my_gn_realloc(result, j)) == NULL)
	return (NULL);
      *buffer = fill_my_buffer(&ret, fd, buffer, &i);
    }
  result[j] = 0;
  return (result);
}
