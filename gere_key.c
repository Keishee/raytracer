/*
** gere_key.c for rt in /home/clement/Dropbox/Raytracer/Clem
** 
** Made by Clément
** Login   <clement@epitech.net>
** 
** Started on  Mon Jun  2 13:45:21 2014 Clément
** Last update Sun Jun  8 18:18:44 2014 barbis_j
*/

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"

int		gere_expose(t_win *win)
{
  mlx_put_image_to_window(win->mlx_ptr,
                          win->win_ptr,
                          win->img_ptr[win->num], 0, 0);
  return (0);
}

int		gere_key_vid(int key, t_win *win)
{
  if (key == LEFT)
    win->num -= 1;
  else if (key == RIGHT)
    win->num += 1;
  else
    return (0);
  if (win->num == -1)
    return (win->num += 1);
  if (win->num == win->num_min_img)
    return (win->num -= 1);
  printf("%s\n", win->str[win->num]);
  gere_expose(win);
  return (1);
}

int		gere_key(int key, t_win *win)
{
  if (key == EXIT)
    exit (1);
  else if (win->vid == 0)
    gere_key_vid(key, win);
  else if (win->vid == 1)
    if (key != EXIT)
      {
	win->num = 0;
	while (win->num < win->num_min_img - 1)
	  {
	    ++win->num;
	    gere_expose(win);
	  }
      }
  return (0);
}
