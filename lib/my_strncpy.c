/*
** my_strncpy.c for my_strncpy in /home/barbis_j/rendu/Piscine-C-Jour_06
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Oct  7 09:48:59 2013 barbis_j
** Last update Thu Jun  5 17:32:45 2014 guillaume
*/

#include "my.h"

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  if (n >= my_strlen(src))
    {
      while (src[i] != '\0')
	{
	  dest[i] = src[i];
	  i = i + 1;
	}
      dest[i] = '\0';
      return (dest);
    }
  while (n > 0)
    {
      dest[i] = src[i];
      i = i + 1;
      n = n - 1;
    }
  dest[i] = 0;
  return (dest);
}
