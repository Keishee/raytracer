/*
** rtv1.h for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Feb 10 16:37:59 2014 barbis_j
** Last update Fri Jun  6 15:14:32 2014 da-sil_l
*/

#ifndef RTV1_H_
# define RTV1_H_

# define ESC 65307
# define UP 65362
# define DOWN 65364
# define LEFT 65361
# define RIGHT 65363
# define SPACE 32

typedef struct	s_mlx
{
  void		*init;
  void		*win;
  void		*img;
}		t_mlx;

typedef struct	s_data
{
  int		sizeline;
  int		endian;
  int		bpp;
  char		*data;
}		t_data;

typedef struct	s_color_b
{
  int		r;
  int		g;
  int		b;
}		t_color_b;

typedef struct	s_fullcolors
{
  t_color_b	background;
  t_color_b	sphere;
  t_color_b	plane;
  t_color_b	cylinder;
  t_color_b	cone;
}		t_fullcolors;

typedef struct	s_coord_b
{
  double	x;
  double	y;
  double	z;
}		t_coord_b;

typedef struct	s_delta_b
{
  double	a;
  double	b;
  double	c;
  double	delta;
  double	k1;
  double	k2;
}		t_delta_b;

typedef struct	s_k_b
{
  double	k[4];
}		t_k_b;

typedef struct	s_lum
{
  t_coord_b	int_sphere;
  t_coord_b	int_plan;
  t_coord_b	int_cylindre;
  t_coord_b	int_cone;
}		t_lum;

typedef struct	s_obj
{
  t_lum		lum;
  double	ray;
  t_coord_b	vec;
  t_coord_b	eye;
  t_k_b		k;
}		t_obj;

typedef struct	s_var
{
  t_data	d;
  t_mlx		m;
  t_coord_b	eye;
}		t_var;

void		rtv1_mat_rotz(t_coord_b *, double);
void		rtv1_mat_roty(t_coord_b *, double);
void		rtv1_mat_rotx(t_coord_b *, double);
double		k_select(double, double);
void		light(t_color_b *, t_coord_b *, int);

#endif /* !RTV1_H_ */
