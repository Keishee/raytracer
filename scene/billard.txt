START

EYE
-300
0
0

OTHER
1
1

- ligne blanche
OBJECT
CYLINDRE
0
-150
1
0
0
0
255
255
255
1
0

- bord haut
OBJECT
CYLINDRE
0
300
25
90
0
0
51
0
0
1
0

- bord bas
OBJECT
CYLINDRE
0
-300
25
90
0
0
51
0
0
1
0

- bord gauche
OBJECT
CYLINDRE
0
-400
25
0
0
0
255
0
0
1
0

- bord droit
OBJECT
CYLINDRE
0
400
25
0
0
0
255
0
0
1
0

OBJECT
SPHERE
1
100
0
25
0
0
0
255
0
0
1
0

OBJECT
SPHERE
1
250
-100
25
0
0
0
255
255
0
1
0

OBJECT
CYLINDRE
90
0
15
90
70
0
255
150
10
1
0

OBJECT
SPHERE
1
-200
0
25
0
0
0
255
255
255
1
0

OBJECT
SPHERE
1
175
50
25
0
0
0
255
255
0
1
0

OBJECT
SPHERE
1
175
-50
25
0
0
0
255
0
0
1
0

OBJECT
SPHERE
1
250
0
25
0
0
0
0
0
0
1
0

OBJECT
SPHERE
1
250
100
25
0
0
0
255
0
0
1
0

PLAN
X
0
0
0
0
0
255
0
0
1

SPOT
-500
0
0
255
255
255
