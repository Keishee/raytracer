/*
** parsing_conf.c for  in /home/calvez_k/Dropbox/Raytracer/Bego
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri Apr 18 14:16:45 2014 
** Last update Sun Jun  8 17:43:09 2014 
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "my.h"

void		parsing_spot(t_rtv *rtv, int fd)
{
  rtv->spot.x[rtv->spot.nb] = my_getnbr(get_next_line(fd));
  rtv->spot.y[rtv->spot.nb] = my_getnbr(get_next_line(fd));
  rtv->spot.z[rtv->spot.nb] = my_getnbr(get_next_line(fd));
  rtv->spot.colore[0][rtv->spot.nb] = my_getnbr(get_next_line(fd));
  rtv->spot.colore[1][rtv->spot.nb] = my_getnbr(get_next_line(fd));
  rtv->spot.colore[2][rtv->spot.nb] = my_getnbr(get_next_line(fd));
  rtv->spot.nb = rtv->spot.nb + 1;
}

void		parsing_eye(t_rtv *rtv, int fd)
{
  rtv->eye.x = my_getnbr(get_next_line(fd));
  rtv->eye.y = my_getnbr(get_next_line(fd));
  rtv->eye.z = my_getnbr(get_next_line(fd));
}

void		parsing_other(t_rtv *rtv, int fd)
{
  rtv->other.x = my_getnbr(get_next_line(fd));
  rtv->other.x /= 100;
  rtv->other.shadow = my_getnbr(get_next_line(fd));
}

void		parsing_all(t_rtv *rtv, int fd, int nb, char *line)
{
  if ((my_strcmp(line, "OBJECT")) == 0)
    parsing_object(&rtv[nb], fd);
  else if ((my_strcmp(line, "EYE")) == 0)
    parsing_eye(&rtv[nb], fd);
  else if ((my_strcmp(line, "SPOT")) == 0)
    parsing_spot(&rtv[nb], fd);
  else if ((my_strcmp(line, "PLAN")) == 0)
    parsing_plan(&rtv[nb], fd);
  else if ((my_strcmp(line, "OTHER")) == 0)
    parsing_other(&rtv[nb], fd);
  free(line);
}

void		parsing_conf(t_win *win, int fd, int nb, int flag)
{
  char		*line;
  t_rtv		rtv[win->num_min_img];

  memset_struct_t_rtv(&rtv[nb]);
  if (flag == 9000)
    {
      create_box(win, nb);
      return ;
    }
  while ((line = get_next_line(fd)) != NULL)
    parsing_all(rtv, fd, nb, line);
  rtv[nb].spot.nb -= 1;
  if (win->vid == 0)
    parcour_img(&rtv[nb], win, 0);
  else if (win->vid == 1)
    make_video(&rtv[0], win);
}
