/*
** make_video.c for  in /home/calvez_k/Dropbox/Raytracer/Bego_l_ombrageux
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Sat May 24 17:01:46 2014 
** Last update Sun Jun  8 18:26:36 2014 barbis_j
*/

#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include "my.h"

void		make_video(t_rtv *rtv, t_win *win)
{
  while (win->nb < NB_VIDEO)
    {
      loading(NB_VIDEO, win->nb, 1, 0);
      new_img(win);
      parcour_img(rtv, win, 1);
      if (rtv->sphere.y[0] <= WINX)
	{
	  rtv->sphere.y[0] = rtv->sphere.y[0] + 1.0;
	  rtv->sphere.z[0] = rtv->sphere.z[0] + 1.0;
	}
      win->nb = win->nb + 1;
    }
  win->num_min_img = win->nb;
}

void		init_video(t_win *win)
{
  int		fd;

  if ((fd = open("scene/video/video.txt", O_RDONLY)) == -1)
    exit(1);
  win->vid = 1;
  parsing_conf(win, fd, 0, 0);
}
