/*
** my_spot.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Thu Mar  6 21:38:21 2014 
** Last update Sun Jun  8 15:23:04 2014 
*/

#include <math.h>
#include "my.h"

void		set_normal(t_rtv *rtv)
{
  if (rtv->color.d == 1 && rtv->color.f == 0)
    {
      rtv->normal.x[0] = 0;
      rtv->normal.y[0] = 0;
      rtv->normal.y[0] = rtv->normal.y[0] + cos(rtv->eye.y / 10) *
      	((sqrt(pow(rtv->normal.x[0], 2) + pow(rtv->normal.y[0], 2) + pow(rtv->normal.z[0], 2))) / 10);
      rtv->normal.z[0] = 100;
    }
}

void		vect_lum(t_rtv *rtv, int x)
{
  rtv->light.x[x] = rtv->spot.x[x] - rtv->lux.x[0];
  rtv->light.y[x] = rtv->spot.y[x] - rtv->lux.y[0];
  rtv->light.z[x] = rtv->spot.z[x] - rtv->lux.z[0];
}

void		set_vect_normal(t_rtv *rtv)
{
  rtv->normal.x[0] = rtv->lux.x[0];
  rtv->normal.y[0] = rtv->lux.y[0];
  rtv->normal.z[0] = rtv->lux.z[0];
}

double		lux(t_rtv *rtv, int x)
{
  double	cos_a;

  vect_lum(rtv, x);
  set_vect_normal(rtv);
  cos_a = (sqrt(pow(rtv->normal.x[0], 2) +
		pow(rtv->normal.y[0], 2) +
		pow(rtv->normal.z[0], 2)));
  cos_a = cos_a * (sqrt(pow(rtv->light.x[x], 2) +
			pow(rtv->light.y[x], 2) +
			pow(rtv->light.z[x], 2)));
  cos_a = cos_a / ((rtv->normal.x[0] * rtv->light.x[x]) +
		   (rtv->normal.y[0] * rtv->light.y[x]) +
		   (rtv->normal.z[0] * rtv->light.z[x]));
  cos_a = 1 / cos_a;
  if (cos_a <= rtv->other.x)
    cos_a = rtv->other.x;
  return (cos_a);
}

void		line_param(t_rtv *rtv)
{
  int		nb;

  rtv->lux.x[0] = rtv->eye.x + rtv->lux.k * rtv->line.vx;
  rtv->lux.y[0]= rtv->eye.y + rtv->lux.k * rtv->line.vy;
  rtv->lux.z[0] = rtv->eye.z + rtv->lux.k * rtv->line.vz;
  nb = -1;
  while (nb++ <= rtv->spot.nb)
    rtv->cos.cos[nb] = lux(rtv, nb);
}
