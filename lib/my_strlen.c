/*
** my_strlen.c for my_strlen in /home/bourge_h/Bitbucket/raytracer
** 
** Made by guillaume
** Login   <bourge_h@epitech.net>
** 
** Started on  Thu Jun  5 17:36:14 2014 guillaume
** Last update Thu Jun  5 17:36:37 2014 guillaume
*/

int		my_strlen(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    ++i;
  return (i);
}
