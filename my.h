/*
** my.h for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Mon Feb 10 13:31:50 2014 
** Last update Sun Jun  8 18:26:16 2014 barbis_j
*/

#ifndef MY_H_
# define MY_H_

# define WINX	800
# define WINY	600
# define D	100
# define P	1
# define EXIT	65307
# define LEFT	65361
# define UP	65362
# define RIGHT	65363
# define DOWN	65364
# define NB_IMG 8
# define SPHERE 0
# define PLAN	1
# define CYLINDRE 2
# define CONE	3
# define NB_VIDEO 900

typedef struct	s_win
{
  void		*mlx_ptr;
  void		*win_ptr;
  void		**img_ptr;
  int		*sizeline;
  char		**data;
  void		*img;
  int		*bpp;
  int		*endian;
  int		nb;
  int		num;
  int		vid;
  int		num_min_img;
  char		**str;
}		t_win;

typedef struct	s_color
{
  double	blue;
  double	red;
  double	green;
  double	mat;
  int		d;
  int		f;
}		t_color;

typedef struct	s_line
{
  int		x1;
  int		y1;
  int		z1;
  double	vx;
  double	vy;
  double	vz;
}		t_line;

typedef struct	s_eye
{
  int		x;
  int		y;
  int		z;
}		t_eye;

typedef struct	s_other
{
  double	x;
  int		shadow;
}		t_other;

typedef struct	s_sphere
{
  int		nb;
  double	a;
  double	b;
  double	c;
  double	delta;
  double	k;
  double	rayon[30];
  int		d;
  double	x[30];
  double	y[30];
  double	z[30];
  double	rotx[30];
  double	roty[30];
  double	rotz[30];
  int		red[30];
  int		green[30];
  int		blue[30];
  double	mat[30];
  int		limit[10];
}		t_sphere;

typedef struct	s_plan
{
  int		nbx;
  int		nby;
  int		nbz;
  double	k;
  double	mat[30];
  double	rtx[30];
  double	rty[30];
  double	rtz[30];
  double	px[30];
  double	y[30];
  double	z[30];
  int		red[30];
  int		green[30];
  int		blue[30];
  int		p;
}		t_plan;

typedef struct	s_lux
{
  int		nb;
  double	k;
  double	x[10];
  double	y[10];
  double	z[10];
  int		p;
}		t_lux;

typedef struct	s_cos
{
  int		nb;
  double	x[10];
  double	y[10];
  double	z[10];
  double	color[10];
  double	colore[10][10];
  double	red[10];
  double	green[10];
  double	blue[10];
  double	cos[10];
  double	intensite;
}		t_cos;

typedef struct	s_k
{
  double	k[10];
  double	mat[10];
  int		red[10];
  int		green[10];
  int		blue[10];
}		t_k;

typedef struct	s_rtv
{
  t_k		k;
  t_line	line;
  t_color	color;
  t_sphere	sphere;
  t_sphere	shadow;
  t_sphere	cyl;
  t_sphere	cone;
  t_plan	plan;
  t_lux		lux;
  t_lux		normal;
  t_lux		light;
  t_eye		dot;
  t_eye		eye;
  t_eye		cp;
  t_cos		spot;
  t_cos		cos;
  t_other	other;
}		t_rtv;

double		lux(t_rtv *, int);
char		*get_next_line(const int);
void		*xmalloc(int);
int		my_putnbr(int);
int		gere_key(int, t_win *);
int		gere_expose(t_win *);
int		my_strcmp(const char *, const char *);
void		init_video(t_win *);
void		init_img(t_win *);
int		my_getnbr(char *);
void		memset_struct_t_rtv(t_rtv *);
void		parsing_object(t_rtv *, int);
void		parcour_img(t_rtv *, t_win *, int);
void		parsing_plan(t_rtv *, int);
void		make_video(t_rtv *, t_win *);
void		memset_struct_spot(t_rtv *);
void		memset_struct_cos(t_rtv *);
void		memset_struct_cone(t_rtv *);
void		memset_struct_cyl(t_rtv *);
void		memset_struct_shadow(t_rtv *);
void		memset_struct_dot(t_rtv *);
void		memset_struct_eye(t_rtv *);
void		memset_struct_cp(t_rtv *);
void		memset_struct_normal(t_rtv *);
void		memset_struct_light(t_rtv *);
void		memset_struct_lux(t_rtv *);
void		memset_struct_plan(t_rtv *);
void		memset_struct_color(t_rtv *);
void		memset_struct_line(t_rtv *);
void		memset_struct_limit(t_rtv *);
int		new_img(t_win *);
void		parsing_conf(t_win *, int, int, int);
void		init_rotate(t_rtv *, double, double, double);
void		vision_line(t_rtv *);
void		init_plan(t_rtv *);
void		init_sphere(t_rtv *);
void		init_cone(t_rtv *);
void		init_cyl(t_rtv *);
void		check_all_k(t_rtv *);
void		loading(int, int, int, int);
void		init_shadow(t_rtv *);
void		line_param(t_rtv *);
void		my_choose_color(t_rtv *, double);
int		my_strlen(char *);

#endif /* MY_H_ */
