/*
** init_img.c for rt in /home/clement/Dropbox/Raytracer/Clem
** 
** Made by Clément
** Login   <clement@epitech.net>
** 
** Started on  Mon Jun  2 13:56:26 2014 Clément
** Last update Sun Jun  8 15:14:09 2014 
*/

#include <mlx.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"

void	init_img(t_win *win)
{
  int	fd[win->num_min_img];

  new_img(win);
  if (!my_strcmp(win->str[win->nb], "scene/sample/s3.txt"))
    {
      parsing_conf(win, 0, win->nb, 9000);
      ++win->nb;
      return ;
    }
  if ((fd[win->nb] = open(win->str[win->nb], O_RDONLY)) == -1)
    exit(1);
  parsing_conf(win, fd[win->nb], win->nb, 0);
  if (close(fd[win->nb]) == -1)
    return ;
  ++win->nb;
  printf(" %d\n", win->nb);
}
