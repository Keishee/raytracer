/*
** my_shadow.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Fri Mar 14 15:17:14 2014 
** Last update Sun Jun  8 16:35:07 2014 
*/

#include <math.h>
#include "my.h"

void		two_delta_shadow(t_rtv *rtv)
{
  double        k1;
  double        k2;

  k1 = (-(rtv->shadow.b) + sqrt(rtv->shadow.delta)) / (2 * rtv->shadow.a);
  k2 = (-(rtv->shadow.b) - sqrt(rtv->shadow.delta)) / (2 * rtv->shadow.a);
  if (k1 < k2 && k1 >= 0.00001)
    rtv->shadow.k = k1;
  else if (k2 <= k1 && k2 >= 0.00001 && k2 <= 1.0000)
    rtv->shadow.k = k2;
  if (rtv->shadow.k == rtv->lux.k)
    return (0);
  if (rtv->shadow.k < 1.0000 && rtv->shadow.k > 0.00001)
    {
      rtv->color.red = rtv->color.red * 0.6;
      rtv->color.blue = rtv->color.blue * 0.6;
      rtv->color.green = rtv->color.green * 0.6;
    }
}

void		shadow_sphere(t_rtv *rtv, int nb, double rayon)
{
  rtv->cp.x = rtv->light.x[rtv->light.p] + rtv->sphere.x[nb];
  rtv->cp.y = rtv->light.y[rtv->light.p] + rtv->sphere.y[nb];
  rtv->cp.z = rtv->light.z[rtv->light.p] + rtv->sphere.z[nb];
  rtv->shadow.a = (pow(rtv->cp.x, 2) +
		   pow(rtv->cp.y, 2) +
		   pow(rtv->cp.z, 2));
  rtv->shadow.b = 2 * (((rtv->lux.x[0] + rtv->sphere.x[nb]) * rtv->cp.x) +
		       ((rtv->lux.y[0] + rtv->sphere.y[nb]) * rtv->cp.y) +
		       ((rtv->lux.z[0] + rtv->sphere.z[nb]) * rtv->cp.z));
  rtv->shadow.c = (pow(rtv->lux.x[0] + rtv->sphere.x[nb], 2) +
		   pow(rtv->lux.y[0] + rtv->sphere.y[nb], 2) +
		   pow(rtv->lux.z[0] + rtv->sphere.z[nb], 2) -
		   rayon);
  rtv->shadow.delta = pow(rtv->shadow.b, 2) -
    4 * rtv->shadow.a * rtv->shadow.c;
  if (rtv->shadow.delta >= 0)
      two_delta_shadow(rtv);
}

void		shadow_cylindre(t_rtv *rtv,int nb, int rayon)
{
  rtv->cp.x = rtv->light.x[rtv->light.p] + rtv->cyl.x[nb];
  rtv->cp.y = rtv->light.y[rtv->light.p] + rtv->cyl.y[nb];
  rtv->shadow.a = (pow(rtv->cp.x, 2) + pow(rtv->cp.y, 2));
  rtv->shadow.b = 2 * (((rtv->lux.x[0] + rtv->cyl.x[nb]) * rtv->cp.x) +
		       ((rtv->lux.y[0] + rtv->cyl.y[nb]) * rtv->cp.y));
  rtv->shadow.c = (pow(rtv->lux.x[0] + rtv->cyl.x[nb], 2) +
		   pow(rtv->lux.y[0] + rtv->cyl.y[nb], 2) -
		   rayon);
  rtv->shadow.delta = pow(rtv->shadow.b, 2) -
    4 * rtv->shadow.a * rtv->shadow.c;
  if (rtv->shadow.delta >= 0)
      two_delta_shadow(rtv);
}

void		shadow_cone(t_rtv *rtv, int nb, double rayon)
{
  rtv->cp.x = rtv->light.x[rtv->light.p] + rtv->cone.x[nb];
  rtv->cp.y = rtv->light.y[rtv->light.p] + rtv->cone.y[nb];
  rtv->cp.z = rtv->light.z[rtv->light.p] + rtv->cone.z[nb];
  rtv->shadow.a = (pow(rtv->cp.x, 2) +
		   pow(rtv->cp.y, 2) -
		   pow(rtv->cp.z, 2) * (rayon / 10));
  rtv->shadow.b = 2 * ((rtv->lux.x[0] + rtv->cone.x[nb]) * rtv->cp.x +
  		       (rtv->lux.y[0] + rtv->cone.y[nb]) * rtv->cp.y +
  		       (rtv->lux.z[0] + rtv->cone.z[nb]) * rtv->cp.z);
  rtv->shadow.c = (pow(rtv->lux.x[0] + rtv->cone.x[nb], 2) +
  		   pow(rtv->lux.y[0] + rtv->cone.y[nb], 2) +
  		   pow(rtv->lux.z[0] + rtv->cone.z[nb], 2) * (rayon / 10));
  rtv->shadow.delta = pow(rtv->shadow.b, 2) -
    (4.0 * rtv->shadow.a * rtv->shadow.c);
  if (rtv->shadow.delta >= 0)
    two_delta_shadow(rtv);
}

void		init_shadow(t_rtv *rtv)
{
  int		num_spot;
  int		nb;

  num_spot = 0;
  while (num_spot <= rtv->spot.nb)
    {
      rtv->light.p = num_spot;
      nb = 0;
      while (nb < rtv->sphere.nb)
      	shadow_sphere(rtv, nb++, rtv->sphere.rayon[nb]);
      nb = 0;
      while (nb < rtv->cone.nb)
      	shadow_cone(rtv, nb++, rtv->cone.rayon[nb]);
      nb = 0;
      while (nb < rtv->cyl.nb)
	shadow_cylindre(rtv, nb++, rtv->cyl.rayon[nb]);
      num_spot += 1;
    }
}
