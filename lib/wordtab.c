/*
** wordtab.c for wordtab in /home/barbis_j/Documents/Projets/wordtab
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Dec  9 14:02:38 2013 barbis_j
** Last update Mon Jun  2 14:15:25 2014 Clément
*/

#include <stdlib.h>
#include "wordtab.h"

static int	wordcount(char *str)
{
  if (str[0] == '\0')
    return (0);
  if (COND(str[0]))
    return (wordcount(&str[1]));
  if (COND(str[1]) || str[1] == '\0')
    return (1 + wordcount(&str[1]));
  return (wordcount(&str[1]));
}

static int	wordlength(char *str)
{
  if (str[0] == 0)
    return (0);
  if (COND(str[0]))
    return (0);
  return (1 + wordlength(&str[1]));
}

static char	*worddup(char *str, int length)
{
  char		*dest;

  dest = xmalloc(length + 1);
  my_strncpy(dest, str, length);
  return (dest);
}

char	**wordtab(char *str)
{
  char	**tab;
  int	i;
  int	m;
  int	len;

  i = 0;
  m = 0;
  tab = xmalloc((wordcount(str) + 1) * sizeof(char *));
  while (str[i] != 0)
    {
      if ((len = wordlength(&str[i])) != 0)
	{
	  tab[m++] = worddup(&str[i], len);
	  i = i + len;
	}
      else
	i = i + 1;
    }
  tab[m] = NULL;
  return (tab);
}
