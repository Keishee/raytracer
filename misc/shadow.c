/*
** shadow.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar 14 19:22:15 2014 barbis_j
** Last update Fri Jun  6 15:18:56 2014 da-sil_l
*/

#include <math.h>
#include "rtv1.h"

void		init_shadow_sphere(t_coord_b *eye_tmp, t_coord_b *vec_tmp,
				   t_coord_b *p, t_coord_b *vlum)
{
  eye_tmp->x = p->x;
  eye_tmp->y = p->y;
  eye_tmp->z = p->z;
  vec_tmp->x = vlum->x;
  vec_tmp->y = vlum->y;
  vec_tmp->z = vlum->z;
}

void		shadow_changecolor_sphere(t_color_b *color)
{
  color->r = color->r * 0.2;
  color->g = color->g * 0.2;
  color->b = color->b * 0.2;
}

int		shadow_sphere_b(t_color_b *color, t_coord_b *p, t_coord_b *vlum)
{
  t_delta_b	delta;
  double	ksphere;
  t_coord_b	eye_tmp;
  t_coord_b	vec_tmp;

  init_shadow_sphere(&eye_tmp, &vec_tmp, p, vlum);
  delta.a = pow(vec_tmp.x, 2) + pow(vec_tmp.y, 2) + pow(vec_tmp.z, 2);
  delta.b = 2 * (eye_tmp.x * vec_tmp.x + eye_tmp.y *
		 vec_tmp.y + eye_tmp.z * vec_tmp.z);
  delta.c = pow(eye_tmp.x, 2) + pow(eye_tmp.y, 2) +
    pow(eye_tmp.z, 2) - pow(100.0, 2);
  delta.delta = pow(delta.b, 2) - 4 * delta.a * delta.c;
  if (delta.delta >= 0)
    {
      delta.k1 = (-delta.b + sqrt(delta.delta)) / (2.0 * delta.a);
      delta.k2 = (-delta.b - sqrt(delta.delta)) / (2.0 * delta.a);
    }
  else
    return (-1);
  ksphere = k_select(delta.k1, delta.k2);
  if (ksphere > 0.0 && ksphere <= 1.0)
    shadow_changecolor_sphere(color);
}

int		shadow_cylindre_b(t_color_b *color, t_coord_b *p,
				  t_coord_b *vlum)
{
  double	a;
  double	b;
  double	c;
  double	delta;
  double	k1;
  double	k2;
  double	kcylindre;
  t_coord_b	eye_tmp;
  t_coord_b	vec_tmp;

  eye_tmp.x = p->x;
  eye_tmp.y = p->y;
  vec_tmp.x = vlum->x;
  vec_tmp.y = vlum->y;
  a = pow(vec_tmp.x, 2) + pow(vec_tmp.y, 2);
  b = 2.0 * (eye_tmp.x * vec_tmp.x + eye_tmp.y * vec_tmp.y);
  c = pow(eye_tmp.x, 2) + pow(eye_tmp.y, 2) - pow(100.0, 2);
  delta = pow(b, 2) - 4.0 * a * c;
  if (delta >= 0)
    {
      k1 = (-b + sqrt(delta)) / (2.0 * a);
      k2 = (-b - sqrt(delta)) / (2.0 * a);
    }
  else
    return (-1);
  kcylindre = k_select(k1, k2);
  if (kcylindre > 0.0 && kcylindre <= 1.0)
    {
      color->r = color->r * 0.2;
      color->g = color->g * 0.2;
      color->b = color->b * 0.2;
    }
}
