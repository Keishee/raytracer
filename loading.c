/*
** loading.c for loading in /home/da-sil_l/Test/countdown
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Wed May 28 09:08:11 2014 da-sil_l
** Last update Thu Jun  5 17:23:48 2014 guillaume
*/

#include <unistd.h>
#include "my.h"

void		loading(int winy, int y, int flag, int vid)
{
  static int	cent = 0;
  static int	count = 0;
  static int	ok = 0;

  if (flag == 1)
    {
      cent = winy / 100;
      if (cent != 0)
	ok = 1;
    }
  if (ok == 0 || vid == 1)
    return ;
  else if (count == 100)
    count = 0;
  if (y != 0 && y % cent == 0 && count != 100)
    {
      ++count;
      write(1, "\r", 1);
      my_putnbr(count);
      write(1, "%", 1);
    }
}
