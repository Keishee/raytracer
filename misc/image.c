/*
** image.c<2> for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Feb 10 16:40:13 2014 barbis_j
** Last update Sun Jun  8 15:27:49 2014 
*/

#include <stdio.h>
#include <math.h>
#include "rtv1.h"
#include "my.h"

int		mlx_put_img(int x, int y, t_win *data, t_color_b *color)
{
  unsigned int	i;

  i = (y * data->sizeline[data->nb]) + (x * 4);
  if ((i + 5) > data->sizeline[data->nb] * WINY)
    return (-1);
  data->data[data->nb][i++] = color->b;
  data->data[data->nb][i++] = color->g;
  data->data[data->nb][i++] = color->r;
  data->data[data->nb][i] = 0;
}

void		init_colors(t_fullcolors *fullcolors)
{
  fullcolors->background.r = 0;
  fullcolors->background.g = 0;
  fullcolors->background.b = 0;
  fullcolors->sphere.r = 255;
  fullcolors->sphere.g = 255;
  fullcolors->sphere.b = 0;
  fullcolors->plane.b = 255;
  fullcolors->plane.r = 0;
  fullcolors->plane.g = 0;
  fullcolors->cylinder.b = 133;
  fullcolors->cylinder.r = 199;
  fullcolors->cylinder.g = 20;
  fullcolors->cone.r = 0;
  fullcolors->cone.b = 0;
  fullcolors->cone.g = 255;
}

void		create_light(t_fullcolors *fullcolors, t_lum *lum)
{
  light(&(fullcolors->plane), &(lum->int_plan), 0);
  light(&(fullcolors->sphere), &(lum->int_sphere), 1);
  light(&(fullcolors->cylinder), &(lum->int_cylindre), 1);
  light(&(fullcolors->cone), &(lum->int_cone), 1);
}

void		create_object(t_win *box, t_fullcolors *fullcolors,
			      int x, int y)
{
  t_lum		lum;
  int		ret;

  init_colors(fullcolors);
  ret = rtv1_calc(x, y, &lum);
  create_light(fullcolors, &lum);
  if (ret == SPHERE)
    mlx_put_img(x, y, box, &fullcolors->sphere);
  else if (ret == PLAN)
    mlx_put_img(x, y, box, &fullcolors->plane);
  else if (ret == CYLINDRE)
    mlx_put_img(x, y, box, &fullcolors->cylinder);
  else if (ret == CONE)
    mlx_put_img(x, y, box, &fullcolors->cone);
  else
    mlx_put_img(x, y, box, &fullcolors->background);
}

void		create_box(t_win *win, int nb)
{
  t_fullcolors	fullcolors;
  int		x;
  int		y;
  int		percent;

  x = 0;
  percent = 0;
  init_colors(&fullcolors);
  loading(WINX, x, 1, 0);
  while (x < WINX)
    {
      y = 0;
      while (y < WINY)
	{
	  create_object(win, &fullcolors, x, y);
	  ++y;
	}
      ++x;
      loading(WINX, x, 0, 0);
    }
  printf(" %d\n", win->nb + 1);
}
