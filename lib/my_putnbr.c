/*
** my_putnbr.c for my_putnbr in /home/barbis_j/Documents/revise
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Oct 25 14:36:44 2013 barbis_j
** Last update Mon Jun  2 14:12:26 2014 Clément
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putnbr(int num)
{
  if (num < 0)
    {
      my_putchar('-');
      num = num * (-1);
    }
  if ((num / 10) > 0)
    my_putnbr(num / 10);
  my_putchar((num % 10) + 48);
}
