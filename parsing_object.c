/*
** parsing_object.c for  in /home/calvez_k/Dropbox/Raytracer/Bego
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Tue Apr 22 06:38:58 2014 
** Last update Sun Jun  8 15:19:34 2014 
*/

#include <math.h>
#include "my.h"

void		parsing_sphere(t_rtv *rtv, int fd)
{
  rtv->sphere.x[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.y[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.z[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.rayon[rtv->sphere.nb] = pow(my_getnbr(get_next_line(fd)), 2);
  rtv->sphere.rotx[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.roty[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.rotz[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.red[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.green[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.blue[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.mat[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->sphere.mat[rtv->sphere.nb] /= 10;
  rtv->sphere.nb = rtv->sphere.nb + 1;
}

void		parsing_cone(t_rtv *rtv, int fd)
{
  rtv->cone.x[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.y[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.z[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.rayon[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.rotx[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.roty[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.rotz[rtv->sphere.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.red[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.green[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.blue[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.mat[rtv->cone.nb] = my_getnbr(get_next_line(fd));
  rtv->cone.mat[rtv->cone.nb] /= 10;
  rtv->cone.nb = rtv->cone.nb + 1;
}

void		parsing_cyl(t_rtv *rtv, int fd)
{
  rtv->cyl.x[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.y[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.rayon[rtv->cyl.nb] = pow(my_getnbr(get_next_line(fd)), 2);
  rtv->cyl.rotx[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.roty[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.rotz[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.red[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.green[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.blue[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.mat[rtv->cyl.nb] = my_getnbr(get_next_line(fd));
  rtv->cyl.mat[rtv->cyl.nb] /= 10;
  rtv->cyl.nb = rtv->cyl.nb + 1;
}

void		parsing_object(t_rtv *rtv, int fd)
{
  char		*line;

  line = get_next_line(fd);
  if ((my_strcmp(line, "SPHERE")) == 0)
    parsing_sphere(rtv, fd);
  else if ((my_strcmp(line, "CONE")) == 0)
    parsing_cone(rtv, fd);
  else if ((my_strcmp(line, "CYLINDRE")) == 0)
    parsing_cyl(rtv, fd);
}
