/*
** memset_struct4.c for rt in /home/clement/Dropbox/Raytracer/Clem
** 
** Made by Clément
** Login   <clement@epitech.net>
** 
** Started on  Mon Jun  2 14:08:14 2014 Clément
** Last update Sun Jun  8 15:19:59 2014 
*/

#include "my.h"

void	memset_struct_lux(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->lux.nb = 0;
  rtv->lux.k = 0;
  rtv->lux.p = 0;
  while(nb <= 10)
    {
      rtv->lux.x[nb] = 0;
      rtv->lux.y[nb] = 0;
      rtv->lux.z[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_plan(t_rtv *rtv)
{
  int	nb;

  nb = 0;
  rtv->plan.nbx = 0;
  rtv->plan.nby = 10;
  rtv->plan.nbz = 20;
  rtv->plan.k = 0;
  rtv->plan.p = 0;
  while(nb <= 30)
    {
      rtv->plan.px[nb] = 0;
      rtv->plan.y[nb] = 0;
      rtv->plan.z[nb] = 0;
      rtv->plan.rtx[nb] = 0;
      rtv->plan.rty[nb] = 0;
      rtv->plan.rtz[nb] = 0;
      rtv->plan.red[nb] = 0;
      rtv->plan.green[nb] = 0;
      rtv->plan.blue[nb] = 0;
      nb += 1;
  }
}

void	memset_struct_color(t_rtv *rtv)
{
  rtv->color.blue = 0;
  rtv->color.red = 0;
  rtv->color.green = 0;
  rtv->color.mat = 0;
  rtv->color.d = 0;
  rtv->color.f = 0;
}

void	memset_struct_line(t_rtv *rtv)
{
  rtv->line.x1 = 0;
  rtv->line.y1 = 0;
  rtv->line.z1 = 0;
  rtv->line.vx = 0;
  rtv->line.vy = 0;
  rtv->line.vz = 0;
}
