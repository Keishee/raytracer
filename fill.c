/*
** fill.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Tue Feb 11 14:24:39 2014 
** Last update Sun Jun  8 15:19:43 2014 
*/

#include "my.h"

void		vision_line(t_rtv *rtv)
{
  rtv->line.x1 = D;
  rtv->line.y1 = (WINX / 2) - rtv->dot.x;
  rtv->line.z1 = (WINY / 2) - rtv->dot.y;
  rtv->line.vx = rtv->line.x1 - rtv->eye.x;
  rtv->line.vy = rtv->line.y1 - rtv->eye.y;
  rtv->line.vz = rtv->line.z1 - rtv->eye.z;
}

void		calc(t_rtv *rtv)
{
  int		nb;

  nb = 0;
  rtv->color.f = 2;
  rtv->color.d = 0;
  while (nb <= 3)
    rtv->k.k[nb++] = 1000;
  rtv->shadow.k = 1000;
  vision_line(rtv);
  init_plan(rtv);
  init_sphere(rtv);
  init_cone(rtv);
  init_cyl(rtv);
  while (nb <= 0)
    if (rtv->k.k[nb] == 1000)
      rtv->k.k[nb--] = -1;
  check_all_k(rtv);
}

void		my_obj_put_to_img(t_rtv *rtv, t_win *win)
{
  unsigned long	i;

  i = (rtv->dot.y * win->sizeline[win->nb]) + (rtv->dot.x * 4);
  win->data[win->nb][i++] = rtv->color.blue;
  win->data[win->nb][i++] = rtv->color.green;
  win->data[win->nb][i++] = rtv->color.red;
  win->data[win->nb][i] = 0;
}

void		parcour_img(t_rtv *rtv, t_win *win, int vid)
{
  rtv->dot.y = 0;
  loading(WINY, rtv->dot.y, 1, vid);
  while (rtv->dot.y <= WINY)
    {
      rtv->dot.x = 0;
      while (rtv->dot.x <= WINX)
	{
	  calc(rtv);
	  my_obj_put_to_img(rtv, win);
	  rtv->dot.x = rtv->dot.x + 1;
	}
      rtv->dot.y = rtv->dot.y + 1;
      loading(WINY, rtv->dot.y, 0, vid);
    }
}
