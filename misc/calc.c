/*
** calc.c for rtv1 in /home/barbis_j/Documents/Projets/MUL_2013_rtv1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar 14 17:08:01 2014 barbis_j
** Last update Sat Jun  7 12:11:29 2014 
*/

#include "my.h"
#include "rtv1.h"

int		rtv1_calc(int x, int y, t_lum *lum)
{
  t_coord_b	proj;
  t_obj		obj;

  obj.ray = 100.0;
  obj.eye.x = -200.0;
  obj.eye.y = 0.0;
  obj.eye.z = 200.0;
  proj.x = D;
  proj.y = (WINX / 2.0 - (double)x);
  proj.z = (WINY / 2.0 - (double)y);
  obj.vec.x = proj.x;
  obj.vec.y = proj.y;
  obj.vec.z = proj.z;
  rtv1_sphere(&obj, lum);
  rtv1_cylindre(&obj, lum);
  return (k_min(&obj));
}

