/*
** parsing_plan.c for  in /home/calvez_k/Dropbox/Raytracer/Bego
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Sat Apr 26 20:47:05 2014 
** Last update Sun Jun  8 18:22:32 2014 
*/

#include "my.h"

void		parsing_plan_x(t_rtv *rtv, int fd)
{
  rtv->plan.px[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.rtx[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.rty[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.rtz[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.red[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.green[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.blue[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.mat[rtv->plan.nbx] = my_getnbr(get_next_line(fd));
  rtv->plan.mat[rtv->plan.nbx] /= 10;
  rtv->plan.nbx = rtv->plan.nbx + 1;
}
void		parsing_plan_y(t_rtv *rtv, int fd)
{
  rtv->plan.y[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.rtx[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.rty[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.rtz[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.red[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.green[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.blue[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.mat[rtv->plan.nby] = my_getnbr(get_next_line(fd));
  rtv->plan.mat[rtv->plan.nby] /= 10;
  rtv->plan.nby = rtv->plan.nby + 1;
}

void		parsing_plan_z(t_rtv *rtv, int fd)
{
  rtv->plan.z[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.rtx[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.rty[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.rtz[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.red[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.green[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.blue[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.mat[rtv->plan.nbz] = my_getnbr(get_next_line(fd));
  rtv->plan.mat[rtv->plan.nbz] /= 10;
  rtv->plan.nbz = rtv->plan.nbz + 1;
}

void		parsing_plan(t_rtv *rtv, int fd)
{
  char		*line;

  line = get_next_line(fd);
  if ((my_strcmp(line, "X")) == 0)
    parsing_plan_x(rtv, fd);
  else if ((my_strcmp(line, "Y")) == 0)
    parsing_plan_y(rtv, fd);
  else if ((my_strcmp(line, "Z")) == 0)
    parsing_plan_z(rtv, fd);
}
