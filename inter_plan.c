/*
** inter_plan.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Tue Apr  1 14:19:39 2014 
** Last update Sat Jun  7 20:57:25 2014 
*/

#include <math.h>
#include "my.h"

void		k_plan(t_rtv *rtv, double k, int nb)
{
  if (k < 0.00001)
    ;
  else if (k <= rtv->k.k[PLAN])
    {
      rtv->k.k[PLAN] = k;
      rtv->k.mat[PLAN] = rtv->plan.mat[nb];
      rtv->k.red[PLAN] = rtv->plan.red[nb];
      rtv->k.green[PLAN] = rtv->plan.green[nb];
      rtv->k.blue[PLAN] = rtv->plan.blue[nb];
    }
}

int		plan_x(t_rtv *rtv, int nb)
{
  double	k;
  double	eye_x_temp;

  eye_x_temp = rtv->eye.x + rtv->plan.px[nb];
  if (rtv->line.vx == 0)
    return (-1);
  k = (-eye_x_temp / rtv->line.vx);
  k_plan(rtv, k, nb);
  return (0);
}

int		plan_y(t_rtv *rtv, int nb)
{
  double	k;
  double	eye_y_temp;

  eye_y_temp = rtv->eye.y + rtv->plan.y[nb];
  if (rtv->line.vy == 0)
    return (-1);
  k = (-eye_y_temp / rtv->line.vy);
  k_plan(rtv, k, nb);
  return (0);
}

int		plan_z(t_rtv *rtv, int nb)
{
  double	k;
  double	eye_z_temp;

  eye_z_temp = rtv->eye.z + rtv->plan.z[nb];
  if (rtv->line.vz == 0)
    return (-1);
  k = (-eye_z_temp / rtv->line.vz);
  k_plan(rtv, k, nb);
  return (0);
}

void		init_plan(t_rtv *rtv)
{
  int		nb;

  nb = 0;
  while (nb < rtv->plan.nbx)
    {
      init_rotate(rtv, rtv->plan.rtx[nb], rtv->plan.rty[nb], rtv->plan.rtz[nb]);
      plan_x(rtv, nb++);
      vision_line(rtv);
    }
  nb = 10;
  while (nb < rtv->plan.nby)
    {
      init_rotate(rtv, rtv->plan.rtx[nb], rtv->plan.rty[nb], rtv->plan.rtz[nb]);
      plan_y(rtv, nb++);
      vision_line(rtv);
    }
  nb = 20;
  while (nb < rtv->plan.nbz)
    {
      init_rotate(rtv, rtv->plan.rtx[nb], rtv->plan.rty[nb], rtv->plan.rtz[nb]);
      plan_z(rtv, nb++);
      vision_line(rtv);
    }
}
