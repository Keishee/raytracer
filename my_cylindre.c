/*
** my_cylindre.c for  in /home/calvez_k/rendu/MUL_2013_rtv1
** 
** Made by 
** Login   <calvez_k@epitech.net>
** 
** Started on  Thu Mar 13 11:02:20 2014 
** Last update Fri Jun  6 20:56:37 2014 
*/

#include <math.h>
#include "my.h"

void		two_delta_cyl(t_rtv *rtv, int nb)
{
  double        k1;
  double        k2;
  double        k;

  k = 2000;
  k1 = (((-1) * rtv->cyl.b) + sqrt(rtv->cyl.delta)) / (2 * rtv->cyl.a);
  k2 = (((-1) *rtv->cyl.b) - sqrt(rtv->cyl.delta)) / (2 * rtv->cyl.a);
  if (k1 < k2 && k1 >= 0.00001)
    k = k1;
  else if (k2 < k1 && k2 >= 0.00001)
    k = k2;
  if (k < rtv->k.k[CYLINDRE])
    {
      rtv->k.k[CYLINDRE] = k;
      rtv->k.mat[CYLINDRE] = rtv->cyl.mat[nb];
      rtv->k.red[CYLINDRE] = rtv->cyl.red[nb];
      rtv->k.green[CYLINDRE] = rtv->cyl.green[nb];
      rtv->k.blue[CYLINDRE] = rtv->cyl.blue[nb];
    }
}

void		fct_cyl(t_rtv *rtv, int nb, double rayon)
{
  rtv->cp.x = rtv->line.vx + rtv->cyl.x[nb];
  rtv->cp.y = rtv->line.vy + rtv->cyl.y[nb];
  rtv->cyl.a = (pow(rtv->cp.x, 2) + pow(rtv->cp.y, 2));
  rtv->cyl.b = 2 * ((rtv->eye.x * rtv->cp.x) + (rtv->eye.y * rtv->cp.y));
  rtv->cyl.c = (pow(rtv->eye.x, 2) + pow(rtv->eye.y, 2) -
		rayon);
  rtv->cyl.delta = pow(rtv->cyl.b, 2) - 4 * rtv->cyl.a * rtv->cyl.c;
  if (rtv->cyl.delta >= 0)
    two_delta_cyl(rtv, nb);
}

void		init_cyl(t_rtv *rtv)
{
  int		nb;

  nb = 0;
  while (nb < rtv->cyl.nb)
    {
      init_rotate(rtv, rtv->cyl.rotx[nb], rtv->cyl.roty[nb], rtv->cyl.rotz[nb]);
      fct_cyl(rtv, nb, rtv->cyl.rayon[nb]);
      vision_line(rtv);
      nb = nb + 1;
    }
}
